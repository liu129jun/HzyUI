﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//
using System.Data;
using System.Collections;
using DbFrame;
using DbFrame.Class;
using Model;

namespace DAL
{
    public class T_FunctionDA
    {
        DBContext db = new DBContext();
        T_Function tf = new T_Function();

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="QuickConditions"></param>
        /// <param name="pageindex"></param>
        /// <param name="pagesize"></param>
        /// <returns></returns>
        public PagingEntity GetDataSource(Hashtable query, int pageindex, int pagesize)
        {
            string where = "";
            var sql = db.Find()
                .Query<T_Function>((a) => new { a.iFunction_Number, a.cFunction_Name, a.cFunction_ByName, a.dFunction_CreateTime, _ukid = a.uFunction_ID })
                .Where(where)
                .ToSQL();

            var pe = db.Find(sql, " iFunction_Number ", true, pageindex, pagesize);
            return new ToJson().GetPagingEntity(pe, new List<BaseEntity>()
            {
                new T_Function()
            });
        }


    }
}
