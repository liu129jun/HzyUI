﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//
using System.Data;
using System.Collections;
using DbFrame;
using DbFrame.Class;
using Utility;
using Model;

namespace DAL
{
    public class T_RolesDA
    {
        DBContext db = new DBContext();
        T_Roles troles = new T_Roles();

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="QuickConditions"></param>
        /// <param name="pageindex"></param>
        /// <param name="pagesize"></param>
        /// <returns></returns>
        public PagingEntity GetDataSource(Hashtable query, int pageindex, int pagesize)
        {
            string where = "";
            where += string.IsNullOrEmpty(query["cRoles_Name"].To_String()) ? "" : " and cRoles_Name like '%" + query["cRoles_Name"].To_String() + "%' ";

            var sql = db.Find().Query<T_Roles>(a => new { a.cRoles_Number, a.cRoles_Name, a.cRoles_Remark, a.dRoles_CreateTime, _ukid = a.uRoles_ID })
                .Where(where)
                .ToSQL();
            var pe = db.Find(sql, " cRoles_Number ", true, pageindex, pagesize);
            return new ToJson().GetPagingEntity(pe, new List<BaseEntity>()
            {
                new T_Roles()
            });
        }


    }
}
