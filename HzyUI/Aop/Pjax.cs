﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//引用
using System.Web.Mvc;
using ExceptionHandle;
using System.Data;
using System.Dynamic;
using System.Web.Script.Serialization;
using DbFrame.Class;

namespace Aop
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class Pjax : ActionFilterAttribute
    {
        /// <summary>
        /// 每次请求Action之前发生，，在行为方法执行前执行
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var isLayout = filterContext.HttpContext.Request.Headers.AllKeys.Contains("IsLayout");
            // var isLayout = filterContext.Controller.TempData["IsLayout"].To_Bool();
            //如果是 当前pjax 页面的母板页  跳过验证 pjax 代码
            if (!isLayout)
            {
                var isPjax = filterContext.HttpContext.Request.Headers.AllKeys.Contains("X-PJAX");
                if (isPjax)
                {

                }
                else
                {
                    var isFindBack = filterContext.HttpContext.Request.QueryString["fun"];
                    //判断是否是查找带回
                    if (isFindBack != "findback")
                    {
                        //pjax 触发了 f5 刷新问题 ，则 触发一下代码
                        filterContext.Result = new RedirectResult(ClassConfig.HomePageUrl) { };
                        //filterContext.HttpContext.Response.End();
                    }

                }
            }

            //base.OnActionExecuting(filterContext);
        }

    }
}