using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//
using DbFrame.Class;
using DbFrame.SQLContext;
using System.Diagnostics;
using System.Linq.Expressions;
using System.Data;
using System.Data.SqlClient;

namespace DbFrame
{
    class Program
    {
        static void Main(string[] args)
        {
            DBContext db = new DBContext();//默认 连接字符串 名称 为 ConnectionString 自动会去 app.config 查找
            DBContext db1 = new DBContext(DataBaseType.MsSql, "你的 链接字符串");//如果 还有 其他库连接 请使用 这种方式
            List<SQL> li = new List<SQL>();//配合 commit 提交 使用

            Stopwatch s = new Stopwatch();
            s.Start();

            List<string> list = new List<string>() { "1", "2", "3" };
            int?[] str = { 1, 2, 3 };
            db.Find<TestT_Users>(item => str.Contains<int?>(item.iUsers_Age));


            //for (int i = 0; i < 1000; i++)
            //{


      /*      DBContext db=new DBContext();//默认sqlserver 数据库 默认链接字符串  ConnectionString 自动会去 app.config 查找
            DBContext db=new DBContext(DataBaseType.MySql);///默认Mysql 数据库 默认链接字符串  ConnectionString 自动会去 app.config 查找
            
            //新增
            var user = new TestT_Users();
            user.cUsers_Name = "123";
            user.cUsers_LoginPwd = "123";
            user.cUsers_LoginName = "123";
            user.cUsers_Email = "123";
            db.Add(user);
            user.uUsers_ID = db.Add(user, true).To_Guid();//第二个参数表示验证字段信息
            if (user.uUsers_ID == Guid.Empty) throw new Exception(db.ErrorMessge);

            db.Add(user, li);//获取 sql  默认存入 li 中
            db.Commit(li);//提交事务
            //插入单个字段
            db.Add(() => new TestT_Users()
            {
                cUsers_Name = "haha"
            });

            //修改
            user = new TestT_Users();
            user.cUsers_Name = "123";
            user.cUsers_LoginPwd = "123";
            user.cUsers_LoginName = "123";
            user.cUsers_Email = "123";
            db.Edit(user, w => w.uUsers_ID == Guid.Empty);//修改所有的字段
            db.Edit(() => new TestT_Users()
            {
                cUsers_Name = "哈哈"
            }, w => w.uUsers_ID == Guid.Empty);

            //删除
            db.Delete<TestT_Users>(w => w.uUsers_ID == Guid.Empty && w.cUsers_Name == null);

            db.Delete<TestT_Users>(w => w.uUsers_ID == Guid.Empty, li);
            db.Commit(li);

            //查询
            var dt = db.Find("select * from " + user.GetTabelName() + " where 1=1");//原始 sql 得到 DataTable

            var model = db.Find<TestT_Users>(w => w.uUsers_ID == Guid.Empty);//单表

            var name = "admin";
            var list = db.FindList<TestT_Users>(w => w.cUsers_LoginName.Like("%" + name + "%"), " dUsers_CreateTime desc ");//单表集合
            //多表查询
            SQLContext.Interface.IQuery iquery = db.Find()
            .Query<TestT_Users, TestMember>((A, B) => new { _ukid = A.uUsers_ID, B.Member_Name })
            .LeftJoin<TestT_Users, TestMember>((A, B) => A.uUsers_ID == B.Member_ID)
            .Where<TestT_Users>(A => A.uUsers_ID == Guid.Empty)
            .OrderBy<TestT_Users>(A => new { A.dUsers_CreateTime, A.cUsers_Name });

            var sql = iquery.ToSQL();//获取sql
            var list_db = iquery.ToList();//获取 字典集合 List<Dictionary<string, object>>
            var dt1 = iquery.ToDataTable();//获取 DataTable

            //其他
            db.FindObject("select count(1) from " + user.GetTabelName() + " where 1=1 and a=b").To_Int();
            DataRow dr = null;//这里只是举例
            db.DataRowToModel<TestT_Users>(dr);//将 DataRow 转换为 实体 TestT_Users
            List<Dictionary<string, object>> newLi = db.FindList("这里 是 sql");

            bool isok = db.CheckModel<TestT_Users>(user);//验证实体 的字段 是否 为空 等等。。。。
            if (!isok) throw new Exception(db.ErrorMessge);//db.ErrorMessge 可能存放 用户名不能为空.....

            var json = db.jss.Serialize(user);//序列化 为 json 串

            //db.dbhelper 原始 数据库访问

             /// <summary>
            /// 查询
            /// </summary>
            /// <param name="QuickConditions"></param>
            /// <param name="pageindex"></param>
            /// <param name="pagesize"></param>
            /// <returns></returns>
            public PagingEntity GetDataSource(Hashtable query, int pageindex, int pagesize)
            {
                string where = "";
                where += string.IsNullOrEmpty(query["cUsers_Name"].To_String()) ? "" : " and cUsers_Name like '%" + query["cUsers_Name"].To_String() + "%' ";
                where += string.IsNullOrEmpty(query["cUsers_LoginName"].To_String()) ? "" : " and cUsers_LoginName like '%" + query["cUsers_LoginName"].To_String() + "%' ";

                var sql = db.Find()
                    .Query<T_Users, T_UsersRoles, T_Roles>((a, b, c) => new { a.cUsers_Name, a.cUsers_LoginName, c.cRoles_Name, a.dUsers_CreateTime, _ukid = a.uUsers_ID })
                    .LeftJoin<T_Users, T_UsersRoles>((a, b) => a.uUsers_ID == b.uUsersRoles_UsersID)
                    .LeftJoin<T_UsersRoles, T_Roles>((b, c) => b.uUsersRoles_RoleID == c.uRoles_ID)
                    .Where(where).ToSQL();
                var pe = db.Find(sql, " dUsers_CreateTime ", false, pageindex, pagesize);
                return new ToJson().GetPagingEntity(pe, new List<BaseEntity>()
                {
                    new T_Users(),
                    new T_Roles()
                });
            }


            //}

        */

            Console.WriteLine(" 耗时：" + (s.ElapsedMilliseconds * 0.001) + " s");
            Console.ReadKey();

        }

    }
}
