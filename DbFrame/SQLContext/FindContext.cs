using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//
using System.Web.Script.Serialization;
using System.Linq.Expressions;
using System.Data;
using DbFrame.AdoDotNet;
using DbFrame.Class;
using DbFrame.SQLContext.Interface;
using System.Data.Common;

namespace DbFrame.SQLContext
{
    public class FindContext : Abstract.AbstractFind
    {
        private string _ConnectionString { get; set; }
        private DbHelper dbhelper { get; set; }
        private DataBaseType DbType { get; set; }
        public FindContext(string ConnectionString, DataBaseType dbtype)
        {
            this._ConnectionString = ConnectionString;
            dbhelper = new DbHelper(ConnectionString, dbtype);
            DbType = dbtype;
        }

        private SQL GetSql<T>(string[] From, Expression<Func<T, bool>> Where, string OrderBy) where T : BaseEntity, new()
        {
            var Model = (T)Activator.CreateInstance(typeof(T));
            string TabName = Model.GetTabelName() + (Where == null ? "" : " " + Where.Parameters[0].Name);
            var pa = new ParserArgs();
            if (Where != null)
                this.GetWhereString<T>(Where, pa);

            var from = new List<string>();
            if (From == null)
                From = new string[] { "*" };
            foreach (var item in From.ToList())
                from.Add(item);

            OrderBy = string.IsNullOrEmpty(OrderBy) ? "" : " ORDER BY " + OrderBy;
            return new SQL(string.Format(" SELECT {0} FROM {1} WHERE 1=1 {2} {3} ", string.Join(",", from), TabName, pa.Builder.ToString(), OrderBy), pa.SqlParameters);
        }

        private DataTable ExecuteSQL<T>(string[] From, Expression<Func<T, bool>> Where, string OrderBy = "") where T : BaseEntity, new()
        {
            var sql = this.GetSql<T>(From, Where, OrderBy);
            return dbhelper.ExecuteDataTable(sql);
        }

        public override IQuery Find()
        {
            return new QueryContext(_ConnectionString, DbType);
        }

        public override DataTable Find(string SQL)
        {
            return dbhelper.ExecuteDataTable(SQL);
        }

        public override object FindObject(string SQL)
        {
            return dbhelper.ExecuteScalar(SQL);
        }

        public override T Find<T>(Expression<Func<T, bool>> Where)
        {
            var dt = this.ExecuteSQL<T>(null, Where);
            if (dt.Rows.Count == 0)
                return (T)Activator.CreateInstance(typeof(T));
            return DbHelper.ToModel<T>(dt.Rows[0]);
        }

        public override DataTable FindTable<T>(Expression<Func<T, bool>> Where, string OrderBy)
        {
            return this.ExecuteSQL<T>(null, Where, OrderBy);
        }

        public override List<T> FindList<T>(Expression<Func<T, bool>> Where, string OrderBy)
        {
            return DbHelper.ConvertDataTableToList<T>(this.ExecuteSQL<T>(null, Where, OrderBy));
        }

        public override List<T> FindListByTable<T>(DataTable dt)
        {
            return DbHelper.ConvertDataTableToList<T>(dt);
        }

        public override List<Dictionary<string, object>> FindList(string SQL)
        {
            return this.FindList(dbhelper.ExecuteDataTable(SQL));
        }

        public override List<Dictionary<string, object>> FindList(DataTable dt)
        {
            return DbHelper.ConvertDataTableToList<Dictionary<string, object>>(dt);
        }

        public override PagingEntity Find(string strSql, string orderField, bool isAsc, int pageIndex, int pageSize, DbParameter[] dbParameter = null)
        {
            return dbhelper.PagingList(strSql, dbParameter, orderField, isAsc, pageIndex, pageSize);
        }


    }
}
