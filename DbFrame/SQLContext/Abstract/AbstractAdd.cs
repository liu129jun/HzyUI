﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//
using DbFrame.SQLContext.Interface;
using DbFrame.Class;
using System.Linq.Expressions;

namespace DbFrame.SQLContext.Abstract
{
    public abstract class AbstractAdd : BaseStrSql, IAdd
    {
        public abstract object Add<T>(T Model, bool IsCheck = false) where T : BaseEntity, new();
        public abstract object Add<T>(Expression<Func<T>> Func) where T : BaseEntity, new();
        public abstract object Add<T>(T Model, List<SQL> li, bool IsCheck = false) where T : BaseEntity, new();
        public abstract object Add<T>(Expression<Func<T>> Func, List<SQL> li) where T : BaseEntity, new();

        object IAdd.Add<T>(T Model, bool IsCheck)
        {
            return Add<T>(Model, IsCheck);
        }

        object IAdd.Add<T>(Expression<Func<T>> Func)
        {
            return Add<T>(Func);
        }

        object IAdd.Add<T>(T Model, List<Class.SQL> li, bool IsCheck)
        {
            return Add<T>(Model, li, IsCheck);
        }

        object IAdd.Add<T>(Expression<Func<T>> Func, List<Class.SQL> li)
        {
            return Add<T>(Func, li);
        }



    }
}
