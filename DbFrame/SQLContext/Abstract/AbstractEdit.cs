﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//
using DbFrame.SQLContext.Interface;
using DbFrame.Class;
using System.Linq.Expressions;

namespace DbFrame.SQLContext.Abstract
{
    public abstract class AbstractEdit : BaseStrSql, IEdit
    {

        public abstract bool Edit<T>(T Set, Expression<Func<T, bool>> Where, bool IsCheck = false) where T : BaseEntity, new();
        public abstract bool Edit<T>(Expression<Func<T>> Set, Expression<Func<T, bool>> Where) where T : BaseEntity, new();
        public abstract bool Edit<T>(T Set, Expression<Func<T, bool>> Where, List<SQL> li, bool IsCheck = false) where T : BaseEntity, new();
        public abstract bool Edit<T>(Expression<Func<T>> Set, Expression<Func<T, bool>> Where, List<SQL> li) where T : BaseEntity, new();


        bool IEdit.Edit<T>(T Set, Expression<Func<T, bool>> Where, bool IsCheck)
        {
            return Edit<T>(Set, Where, IsCheck);
        }

        bool IEdit.Edit<T>(Expression<Func<T>> Set, Expression<Func<T, bool>> Where)
        {
            return Edit<T>(Set, Where);
        }

        bool IEdit.Edit<T>(T Set, Expression<Func<T, bool>> Where, List<Class.SQL> li, bool IsCheck)
        {
            return Edit<T>(Set, Where, li, IsCheck);
        }

        bool IEdit.Edit<T>(Expression<Func<T>> Set, Expression<Func<T, bool>> Where, List<Class.SQL> li)
        {
            return Edit<T>(Set, Where, li);
        }







    }
}
