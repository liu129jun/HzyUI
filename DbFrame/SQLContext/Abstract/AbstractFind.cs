﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//
using DbFrame.SQLContext.Interface;
using DbFrame.Class;
using System.Linq.Expressions;
using System.Data;
using System.Data.Common;

namespace DbFrame.SQLContext.Abstract
{
    public abstract class AbstractFind : BaseStrSql, IFind
    {

        public abstract IQuery Find();
        public abstract DataTable Find(string SQL);
        public abstract object FindObject(string SQL);
        public abstract T Find<T>(Expression<Func<T, bool>> Where) where T : BaseEntity, new();
        public abstract DataTable FindTable<T>(Expression<Func<T, bool>> Where, string OrderBy) where T : BaseEntity, new();
        public abstract List<T> FindList<T>(Expression<Func<T, bool>> Where, string OrderBy) where T : BaseEntity, new();
        public abstract List<T> FindListByTable<T>(DataTable dt) where T : BaseEntity, new();
        public abstract List<Dictionary<string, object>> FindList(string SQL);
        public abstract List<Dictionary<string, object>> FindList(DataTable dt);
        public abstract PagingEntity Find(string strSql, string orderField, bool isAsc, int pageIndex, int pageSize, params DbParameter[] dbParameter);

        IQuery IFind.Find()
        {
            return Find();
        }

        DataTable IFind.Find(string SQL)
        {
            return Find(SQL);
        }

        object IFind.FindObject(string SQL)
        {
            return Find(SQL);
        }

        T IFind.Find<T>(Expression<Func<T, bool>> Where)
        {
            return Find<T>(Where);
        }

        DataTable IFind.FindTable<T>(Expression<Func<T, bool>> Where, string OrderBy)
        {
            return FindTable<T>(Where, OrderBy);
        }

        List<T> IFind.FindList<T>(Expression<Func<T, bool>> Where, string OrderBy)
        {
            return FindList<T>(Where, OrderBy);
        }

        List<T> IFind.FindListByTable<T>(DataTable dt)
        {
            return FindListByTable<T>(dt);
        }

        List<Dictionary<string, object>> IFind.FindList(string SQL)
        {
            return FindList(SQL);
        }

        List<Dictionary<string, object>> IFind.FindList(DataTable dt)
        {
            return FindList(dt);
        }

        PagingEntity IFind.Find(string strSql, string orderField, bool isAsc, int pageIndex, int pageSize, params DbParameter[] dbParameter)
        {
            return this.Find(strSql, orderField, isAsc, pageIndex, pageSize, dbParameter);
        }

    }
}
