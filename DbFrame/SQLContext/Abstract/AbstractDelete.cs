﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//
using DbFrame.SQLContext.Interface;
using DbFrame.Class;
using System.Linq.Expressions;

namespace DbFrame.SQLContext.Abstract
{
    public abstract class AbstractDelete : BaseStrSql, IDelete
    {

        public abstract bool Delete<T>(Expression<Func<T, bool>> Where) where T : BaseEntity, new();
        public abstract bool Delete<T>(Expression<Func<T, bool>> Where, List<SQL> li) where T : BaseEntity, new();


        bool IDelete.Delete<T>(Expression<Func<T, bool>> Where)
        {
            return Delete<T>(Where);
        }

        bool IDelete.Delete<T>(Expression<Func<T, bool>> Where, List<Class.SQL> li)
        {
            return Delete<T>(Where, li);
        }







    }
}
