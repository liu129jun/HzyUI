﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//
using DbFrame.Class;
using System.Linq.Expressions;

namespace DbFrame.SQLContext.Interface
{
    public interface IAdd
    {
        object Add<T>(T Model, bool IsCheck) where T : BaseEntity, new();
        object Add<T>(Expression<Func<T>> Func) where T : BaseEntity, new();
        object Add<T>(T Model, List<SQL> li, bool IsCheck) where T : BaseEntity, new();
        object Add<T>(Expression<Func<T>> Func, List<SQL> li) where T : BaseEntity, new();

    }
}
