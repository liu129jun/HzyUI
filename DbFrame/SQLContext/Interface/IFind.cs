﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//
using DbFrame.Class;
using System.Linq.Expressions;
using System.Data;
using System.Data.Common;

namespace DbFrame.SQLContext.Interface
{
    public interface IFind
    {
        IQuery Find();
        DataTable Find(string SQL);
        object FindObject(string SQL);
        T Find<T>(Expression<Func<T, bool>> Where) where T : BaseEntity, new();
        DataTable FindTable<T>(Expression<Func<T, bool>> Where, string OrderBy) where T : BaseEntity, new();
        List<T> FindList<T>(Expression<Func<T, bool>> Where, string OrderBy) where T : BaseEntity, new();
        List<T> FindListByTable<T>(DataTable dt) where T : BaseEntity, new();
        List<Dictionary<string, object>> FindList(string SQL);
        List<Dictionary<string, object>> FindList(DataTable dt);
        PagingEntity Find(string strSql, string orderField, bool isAsc, int pageIndex, int pageSize, params DbParameter[] dbParameter);

    }
}
