﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//
using DbFrame.Class;
using System.Linq.Expressions;

namespace DbFrame.SQLContext.Interface
{
    public interface IEdit
    {

        bool Edit<T>(T Set, Expression<Func<T, bool>> Where, bool IsCheck) where T : BaseEntity, new();
        bool Edit<T>(Expression<Func<T>> Set, Expression<Func<T, bool>> Where) where T : BaseEntity, new();
        bool Edit<T>(T Set, Expression<Func<T, bool>> Where, List<SQL> li, bool IsCheck) where T : BaseEntity, new();
        bool Edit<T>(Expression<Func<T>> Set, Expression<Func<T, bool>> Where, List<SQL> li) where T : BaseEntity, new();

    }
}
