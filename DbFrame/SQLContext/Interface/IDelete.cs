﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//
using DbFrame.Class;
using System.Linq.Expressions;

namespace DbFrame.SQLContext.Interface
{
    public interface IDelete
    {

        bool Delete<T>(Expression<Func<T, bool>> Where) where T : BaseEntity, new();
        bool Delete<T>(Expression<Func<T, bool>> Where, List<SQL> li) where T : BaseEntity, new();

    }
}
