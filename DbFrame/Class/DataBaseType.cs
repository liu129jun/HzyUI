﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbFrame.Class
{
    public enum DataBaseType
    {
        /// <summary>
        /// SqlServer
        /// </summary>
        MsSql,

        /// <summary>
        /// MySql
        /// </summary>
        MySql,

        /// <summary>
        /// Oracle
        /// </summary>
        Oracle

    }
}
