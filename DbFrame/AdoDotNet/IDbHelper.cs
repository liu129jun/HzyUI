﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//
using DbFrame.Class;
using System.Data.Common;
using System.Data;

namespace DbFrame.AdoDotNet
{
    public interface IDbHelper
    {
        int ExecuteNonQuery(SQL sql);
        DataSet ExecuteDataset(SQL sql);
        DataTable ExecuteDataTable(SQL sql);
        object ExecuteScalar(SQL sql);
        int ExecuteByProc(SQL sql);
        PagingEntity PagingList(string strSql, DbParameter[] dbParameter, string orderField, bool isAsc, int pageIndex, int pageSize);

    }
}
