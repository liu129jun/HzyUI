﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbFrame.AdoDotNet
{
    public class MSSqlDatabase : IDataBase
    {
        private string _ConnectionString { get; set; }

        public MSSqlDatabase(string ConnectionString)
        {
            this._ConnectionString = ConnectionString;
        }

        public bool Commit(List<Class.SQL> li)
        {
            return this.Commit(li, (i, item, cmd) =>
            {
                //执行sql
                cmd.CommandText = item.Sql_Parameter;
                foreach (var par in item.Parameter)
                {
                    cmd.Parameters.Add(new SqlParameter() { ParameterName = par.Key, Value = par.Value == null ? DBNull.Value : par.Value });
                }
                cmd.ExecuteNonQuery();
            });
        }

        public bool Commit(List<Class.SQL> li, Action<int, Class.SQL, System.Data.Common.DbCommand> callBack)
        {
            using (var conn = new SqlConnection(_ConnectionString))
            {
                using (var cmd = new SqlCommand())
                {
                    conn.Open();
                    var tx = conn.BeginTransaction();
                    cmd.Connection = conn;
                    cmd.Transaction = tx;
                    try
                    {
                        li.ForEach(item =>
                        {
                            callBack(li.IndexOf(item), item, cmd);
                            cmd.Parameters.Clear();
                        });
                        //提交事务
                        tx.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        //失败则回滚事务
                        tx.Rollback();
                        throw ex;
                    }
                    finally
                    {
                        conn.Close();
                    }

                }

            }
        }

        /// <summary>
        /// 执行 增/删/改
        /// </summary>
        /// <param name="strSql"></param>
        /// <returns></returns>
        public int ExecuteNonQuery(string strSql)
        {
            return this.ExecuteNonQuery(strSql, null);
        }

        /// <summary>
        /// 执行 增/删/改 参数化
        /// </summary>
        /// <param name="strSql"></param>
        /// <param name="dbParameter"></param>
        /// <returns></returns>
        public int ExecuteNonQuery(string strSql, params System.Data.Common.DbParameter[] dbParameter)
        {
            using (var connection = new SqlConnection(_ConnectionString))
            {
                using (var cmd = new SqlCommand())
                {
                    PrepareCommand(cmd, connection, null, strSql, dbParameter);
                    int rows = cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();
                    return rows;
                }
            }
        }

        /// <summary>
        /// 执行 查询
        /// </summary>
        /// <param name="strSql"></param>
        /// <returns></returns>
        public DataSet ExecuteDataset(string strSql)
        {
            return this.ExecuteDataset(strSql, null);
        }

        /// <summary>
        /// 执行 查询 参数化
        /// </summary>
        /// <param name="strSql"></param>
        /// <param name="dbParameter"></param>
        /// <returns></returns>
        public DataSet ExecuteDataset(string strSql, params System.Data.Common.DbParameter[] dbParameter)
        {
            using (var connection = new SqlConnection(_ConnectionString))
            {
                using (var cmd = new SqlCommand())
                {
                    PrepareCommand(cmd, connection, null, strSql, dbParameter);
                    using (var da = new SqlDataAdapter(cmd))
                    {
                        var ds = new DataSet();
                        da.Fill(ds, "ds");
                        cmd.Parameters.Clear();
                        return ds;
                    }
                }
            }
        }

        /// <summary>
        /// 获取 DataTable
        /// </summary>
        /// <param name="strSql"></param>
        /// <returns></returns>
        public DataTable ExecuteDataTable(string strSql)
        {
            var tabs = this.ExecuteDataset(strSql).Tables;
            if (tabs.Count > 0)
                return tabs[0];
            return null;
        }
        /// <summary>
        /// 获取 DataTable
        /// </summary>
        /// <param name="strSql"></param>
        /// <param name="dbParameter"></param>
        /// <returns></returns>
        public DataTable ExecuteDataTable(string strSql, params DbParameter[] dbParameter)
        {
            var tabs = this.ExecuteDataset(strSql, dbParameter).Tables;
            if (tabs.Count > 0)
                return tabs[0];
            return null;
        }

        public int ExecuteByProc(string procName)
        {
            using (var connection = new SqlConnection(_ConnectionString))
            {

            }
            throw new NotImplementedException();
        }

        public int ExecuteByProc(string procName, System.Data.Common.DbParameter[] dbParameter)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 查询 返回第一行第一列
        /// </summary>
        /// <param name="strSql"></param>
        /// <returns></returns>
        public object ExecuteScalar(string strSql)
        {
            return this.ExecuteScalar(strSql, null);
        }

        /// <summary>
        /// 查询 返回第一行第一列
        /// </summary>
        /// <param name="strSql"></param>
        /// <param name="dbParameter"></param>
        /// <returns></returns>
        public object ExecuteScalar(string strSql, System.Data.Common.DbParameter[] dbParameter)
        {
            using (var connection = new SqlConnection(_ConnectionString))
            {
                using (var cmd = new SqlCommand())
                {
                    PrepareCommand(cmd, connection, null, strSql, dbParameter);
                    var rel = cmd.ExecuteScalar();
                    cmd.Parameters.Clear();
                    return rel;
                }
            }
        }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="strSql">sql 语句</param>
        /// <param name="dbParameter">参数化</param>
        /// <param name="orderField">排序字段</param>
        /// <param name="isAsc">是否正序</param>
        /// <param name="pageSize">一页显示多少条</param>
        /// <param name="pageIndex">页码</param>
        /// <param name="total">总数</param>
        /// <returns></returns>
        public DataTable FindTable(string strSql, DbParameter[] dbParameter, string orderField, bool isAsc, int pageSize, int pageIndex, out int total)
        {
            StringBuilder sb = new StringBuilder();
            if (pageIndex == 0)
            {
                pageIndex = 1;
            }
            int num = (pageIndex - 1) * pageSize;
            int num1 = (pageIndex) * pageSize;
            string OrderBy = "";
            if (!string.IsNullOrEmpty(orderField))
            {
                if (orderField.ToUpper().IndexOf("ASC") + orderField.ToUpper().IndexOf("DESC") > 0)
                {
                    OrderBy = "Order By " + orderField;
                }
                else
                {
                    OrderBy = "Order By " + orderField + " " + (isAsc ? "ASC" : "DESC");
                }
            }
            else
            {
                OrderBy = "order by (select 0)";
            }
            sb.Append("Select * From (Select ROW_NUMBER() Over (" + OrderBy + ")");
            sb.Append(" As rowNum, * From (" + strSql + ") As T ) As N Where rowNum > " + num + " And rowNum <= " + num1 + "");
            total = Convert.ToInt32(this.ExecuteScalar("Select Count(1) From (" + strSql + ") As t", dbParameter));
            var dt = this.ExecuteDataTable(sb.ToString(), dbParameter);
            dt.Columns.Remove("rowNum");
            return dt;
        }

        /// <summary>
        /// 执行ADO.NET SQL时要用的参数添加
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="conn"></param>
        /// <param name="trans"></param>
        /// <param name="cmdText"></param>
        /// <param name="cmdParms"></param>
        private void PrepareCommand(SqlCommand cmd, SqlConnection conn, SqlTransaction trans, string cmdText, DbParameter[] cmdParms)
        {
            if (conn.State != ConnectionState.Open) conn.Open();
            cmd.Connection = conn;
            cmd.CommandText = cmdText;
            if (trans != null)
                cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            if (cmdParms != null)
            {
                foreach (SqlParameter parm in cmdParms) cmd.Parameters.Add(parm);
            }
        }

        /// <summary>
        /// 执行SQL存储过程时要用的参数添加
        /// <returns>SqlDataReader</returns>
        /// 
        private static void PrepareCommand(DbCommand cmd, SqlTransaction trans, IDataParameter[] cmdParms)
        {
            if (trans != null)
                cmd.Transaction = trans;
            if (cmdParms != null)
            {
                foreach (SqlParameter parm in cmdParms) cmd.Parameters.Add(parm);
            }
        }





    }
}
