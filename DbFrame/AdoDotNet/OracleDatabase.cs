﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbFrame.AdoDotNet
{
    public class OracleDatabase : IDataBase
    {


        public bool Commit(List<Class.SQL> li)
        {
            throw new NotImplementedException();
        }

        public bool Commit(List<Class.SQL> li, Action<int, Class.SQL, System.Data.Common.DbCommand> callBack)
        {
            throw new NotImplementedException();
        }

        public int ExecuteNonQuery(string strSql)
        {
            throw new NotImplementedException();
        }

        public int ExecuteNonQuery(string strSql, params System.Data.Common.DbParameter[] dbParameter)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataSet ExecuteDataset(string strSql)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataSet ExecuteDataset(string strSql, params System.Data.Common.DbParameter[] dbParameter)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable ExecuteDataTable(string strSql)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable ExecuteDataTable(string strSql, params System.Data.Common.DbParameter[] dbParameter)
        {
            throw new NotImplementedException();
        }

        public object ExecuteScalar(string strSql)
        {
            throw new NotImplementedException();
        }

        public object ExecuteScalar(string strSql, params System.Data.Common.DbParameter[] dbParameter)
        {
            throw new NotImplementedException();
        }

        public int ExecuteByProc(string procName)
        {
            throw new NotImplementedException();
        }

        public int ExecuteByProc(string procName, System.Data.Common.DbParameter[] dbParameter)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable FindTable(string strSql, System.Data.Common.DbParameter[] dbParameter, string orderField, bool isAsc, int pageSize, int pageIndex, out int total)
        {
            throw new NotImplementedException();
        }






    }
}
