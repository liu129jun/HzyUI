﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Common;
//
using DbFrame.Class;

namespace DbFrame.AdoDotNet
{
    public interface IDataBase
    {


        bool Commit(List<SQL> li);
        bool Commit(List<SQL> li, Action<int, SQL, DbCommand> callBack);
        int ExecuteNonQuery(string strSql);
        int ExecuteNonQuery(string strSql, params DbParameter[] dbParameter);
        DataSet ExecuteDataset(string strSql);
        DataSet ExecuteDataset(string strSql, params DbParameter[] dbParameter);
        DataTable ExecuteDataTable(string strSql);
        DataTable ExecuteDataTable(string strSql, params DbParameter[] dbParameter);
        object ExecuteScalar(string strSql);
        object ExecuteScalar(string strSql, params DbParameter[] dbParameter);
        int ExecuteByProc(string procName);
        int ExecuteByProc(string procName, DbParameter[] dbParameter);
        DataTable FindTable(string strSql, DbParameter[] dbParameter, string orderField, bool isAsc, int pageSize, int pageIndex, out int total);




    }
}
