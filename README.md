开发环境：vs2013、asp.net mvc 4

前端：
BootStrap 3
Layer
Jquery 
后端：
三层+MVC4

还有一个 基于 H+ 的版本:https://github.com/HaoZhiYing/HPlus

--集成通配权限管理

--ORM 大部分使用 拉姆达 表达式 操作

--ORM 举例：

            DBContext db=new DBContext();//默认sqlserver 数据库 默认链接字符串  ConnectionString 自动会去 app.config 查找

            DBContext db=new DBContext(DataBaseType.MySql);///Mysql 数据库 默认链接字符串  ConnectionString 自动会去 app.config 查找
            DBContext db=new DBContext(DataBaseType.MySql,"链接字符串");
            
            //新增
            var user = new TestT_Users();
            user.cUsers_Name = "123";
            user.cUsers_LoginPwd = "123";
            user.cUsers_LoginName = "123";
            user.cUsers_Email = "123";
            db.Add(user);
            user.uUsers_ID = db.Add(user, true).To_Guid();//第二个参数表示验证字段信息
            if (user.uUsers_ID == Guid.Empty) throw new Exception(db.ErrorMessge);

            db.Add(user, li);//获取 sql  默认存入 li 中
            db.Commit(li);//提交事务
            //插入单个字段
            db.Add(() => new TestT_Users()
            {
                cUsers_Name = "haha"
            });

            //修改
            user = new TestT_Users();
            user.cUsers_Name = "123";
            user.cUsers_LoginPwd = "123";
            user.cUsers_LoginName = "123";
            user.cUsers_Email = "123";
            db.Edit(user, w => w.uUsers_ID == Guid.Empty);//修改所有的字段
            db.Edit(() => new TestT_Users()
            {
                cUsers_Name = "哈哈"
            }, w => w.uUsers_ID == Guid.Empty);

            //删除
            db.Delete<TestT_Users>(w => w.uUsers_ID == Guid.Empty && w.cUsers_Name == null);

            db.Delete<TestT_Users>(w => w.uUsers_ID == Guid.Empty, li);
            db.Commit(li);

            //查询
            var dt = db.Find("select * from " + user.GetTabelName() + " where 1=1");//原始 sql 得到 DataTable

            var model = db.Find<TestT_Users>(w => w.uUsers_ID == Guid.Empty);//单表

            var name = "admin";
            var list = db.FindList<TestT_Users>(w => w.cUsers_LoginName.Like("%" + name + "%"), " dUsers_CreateTime desc ");//单表集合
            //多表查询
            SQLContext.Interface.IQuery iquery = db.Find()
            .Query<TestT_Users, TestMember>((A, B) => new { _ukid = A.uUsers_ID, B.Member_Name })
            .LeftJoin<TestT_Users, TestMember>((A, B) => A.uUsers_ID == B.Member_ID)
            .Where<TestT_Users>(A => A.uUsers_ID == Guid.Empty)
            .OrderBy<TestT_Users>(A => new { A.dUsers_CreateTime, A.cUsers_Name });

            var sql = iquery.ToSQL();//获取sql
            var list_db = iquery.ToList();//获取 字典集合 List<Dictionary<string, object>>
            var dt1 = iquery.ToDataTable();//获取 DataTable

            //其他
            db.FindObject("select count(1) from " + user.GetTabelName() + " where 1=1 and a=b").To_Int();
            DataRow dr = null;//这里只是举例
            db.DataRowToModel<TestT_Users>(dr);//将 DataRow 转换为 实体 TestT_Users
            List<Dictionary<string, object>> newLi = db.FindList("这里 是 sql");

            bool isok = db.CheckModel<TestT_Users>(user);//验证实体 的字段 是否 为空 等等。。。。
            if (!isok) throw new Exception(db.ErrorMessge);//db.ErrorMessge 可能存放 用户名不能为空.....

            var json = db.jss.Serialize(user);//序列化 为 json 串

            //db.dbhelper 原始 数据库访问

             /// <summary>
            /// 查询 内置分页查询
            /// </summary>
            /// <param name="QuickConditions"></param>
            /// <param name="pageindex"></param>
            /// <param name="pagesize"></param>
            /// <returns></returns>
            public PagingEntity GetDataSource(Hashtable query, int pageindex, int pagesize)
            {
                string where = "";
                where += string.IsNullOrEmpty(query["cUsers_Name"].To_String()) ? "" : " and cUsers_Name like '%" + query["cUsers_Name"].To_String() + "%' ";
                where += string.IsNullOrEmpty(query["cUsers_LoginName"].To_String()) ? "" : " and cUsers_LoginName like '%" + query["cUsers_LoginName"].To_String() + "%' ";

                var sql = db.Find()
                    .Query<T_Users, T_UsersRoles, T_Roles>((a, b, c) => new { a.cUsers_Name, a.cUsers_LoginName, c.cRoles_Name, a.dUsers_CreateTime, _ukid = a.uUsers_ID })
                    .LeftJoin<T_Users, T_UsersRoles>((a, b) => a.uUsers_ID == b.uUsersRoles_UsersID)
                    .LeftJoin<T_UsersRoles, T_Roles>((b, c) => b.uUsersRoles_RoleID == c.uRoles_ID)
                    .Where(where).ToSQL();
                var pe = db.Find(sql, " dUsers_CreateTime ", false, pageindex, pagesize);
                return new ToJson().GetPagingEntity(pe, new List<BaseEntity>()
                {
                    new T_Users(),
                    new T_Roles()
                });
            }




登录：
![输入图片说明](https://git.oschina.net/uploads/images/2017/0905/163742_4931f031_1242080.png "屏幕截图.png")
首页：
![![![![输入图片说明](https://git.oschina.net/uploads/images/2017/0905/164125_a47cf804_1242080.png "屏幕截图.png")](https://git.oschina.net/uploads/images/2017/0905/164122_63a486ee_1242080.png "屏幕截图.png")](https://git.oschina.net/uploads/images/2017/0905/164116_8e5c80cd_1242080.png "屏幕截图.png")](https://git.oschina.net/uploads/images/2017/0905/164113_8c16b3e2_1242080.png "屏幕截图.png")
皮肤：
![输入图片说明](https://git.oschina.net/uploads/images/2017/0905/164335_eef2a3ad_1242080.png "屏幕截图.png")
列表：
![![输入图片说明](https://git.oschina.net/uploads/images/2017/0905/164706_1da683e4_1242080.png "屏幕截图.png")](https://git.oschina.net/uploads/images/2017/0905/164632_1d4c502f_1242080.png "屏幕截图.png")
添加/修改：
![输入图片说明](https://git.oschina.net/uploads/images/2017/0905/164830_7f50f7c6_1242080.png "屏幕截图.png")
=============手机版本==============
列表：
![输入图片说明](https://git.oschina.net/uploads/images/2017/0905/164919_82beaa78_1242080.png "屏幕截图.png")
添加/修改:
![输入图片说明](https://git.oschina.net/uploads/images/2017/0905/164949_99895558_1242080.png "屏幕截图.png")